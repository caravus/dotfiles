
local popup_toolbar = {}
local popup_toolbar_tasklist = {}




-- {{{ Helper functions
local function list_update(w, buttons, label, data, objects)
    common.list_update(w, buttons, label, data, objects)
    w:set_max_widget_size(64)
end



local function client_menu_toggle_fn()
    local instance = nil

    return function()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({
                theme = { width = 250 }
            })
        end
    end
end


local tasklist_buttons = awful.util.table.join(
        awful.button({ }, 1, function(c)
            if c == client.focus then
                c.minimized = true
            else
                -- Without this, the following
                -- :isvisible() makes no sense
                c.minimized = false
                if not c:isvisible() and c.first_tag then
                    c.first_tag:view_only()
                end
                -- This will also un-minimize
                -- the client, if needed
                client.focus = c
                c:raise()
            end
        end),
        awful.button({ }, 3, client_menu_toggle_fn()),
        awful.button({ }, 4, function()
            awful.client.focus.byidx(1)
        end),
        awful.button({ }, 5, function()
            awful.client.focus.byidx(-1)
        end)
)

local taglist_buttons = awful.util.table.join(
        awful.button({ }, 1, function(t)
            t:view_only()
        end),
        awful.button({ modkey }, 1, function(t)
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end),
        awful.button({ }, 3, awful.tag.viewtoggle),
        awful.button({ modkey }, 3, function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end),
        awful.button({ }, 4, function(t)
            awful.tag.viewnext(t.screen)
        end),
        awful.button({ }, 5, function(t)
            awful.tag.viewprev(t.screen)
        end)
)
-- }}}

function toggle_popup_box(s)
    s.popup_toolbar.visible = not s.popup_toolbar.visible
end



function render_popup_box(s)
   s.popup_toolbar = awful.wibar({
        position = "right",
        screen = s,
        --ontop = true,
        width = 300,
        visible = false,
        --type = "toolbar",
    })

    s.popup_toolbar_tasklist = awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        update_function = list_update,
        style = {
            shape_border_width = 1,
            shape_border_color = '#777777',
            shape = gears.shape.rectangle,
        },
        layout = {
            layout = wibox.layout.flex.vertical
        },
        -- Notice that there is *NO* wibox.wibox prefix, it is a template,
        -- not a widget instance.
        widget_template = {
            {
                {
                    {
                        {
                            id = 'icon_role',
                            widget = wibox.widget.imagebox,
                            forced_width = 64,
                        },
                        margins = 2,
                        widget = wibox.container.margin,
                    },
                    {
                        id = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    -- forced_width = 300,
                    layout = wibox.layout.fixed.horizontal,
                },
                left = 10,
                right = 10,
                widget = wibox.container.margin
            },
            id = 'background_role',
            widget = wibox.container.background,
        },
    }

    s.popup_toolbar:setup {
        layout = wibox.layout.align.vertical,
        {
            layout = wibox.layout.fixed.vertical,
            awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons),
        },
        s.popup_toolbar_tasklist,
        {
            layout = wibox.layout.fixed.vertical,
            wibox.widget.systray(),
            awful.widget.keyboardlayout(),
            wibox.widget.textclock(),
        },
    }
end
